# DCS-CSAR

Simplified Medevac script specifically for pilot rescue.

By default, any crashed plane with an ejected pilot will be disabled until the pilot is rescued and dropped back safely to a friendly MASH
